Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    devise_for :users, :skip => [:sessions, :passwords, :registrations]
    root 'v1/static_pages#status'
    namespace :v1 do
      resources :users, :only => [:create]
      resource :systems, onlu: :none do
        collection do
          post 'validate' => 'systems#validate'
        end
      end
      # resources :sessions, :only => [:create], as: 'login'
      post 'login' => 'sessions#create', as: 'login'
      resources :tokens, :only => [:create] do
        collection do
          post 'validate' => 'tokens#validate'
        end
      end
    end
  end
end
