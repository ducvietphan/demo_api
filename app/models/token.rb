class Token < ActiveRecord::Base
  belongs_to :user

  validates :authorization_code, uniqueness: true, allow_blank: true
  validates :access_token, uniqueness: true, allow_blank: true

  before_create :generate_authorization_code!

  def expired?
    self.expired_in < DateTime.now.utc
  rescue
    true
  end

  def generate_authorization_code!
    begin
      self.authorization_code = Devise.friendly_token
    end while self.class.exists?(authorization_code: authorization_code)
  end

  def generate_access_token!
    begin
      self.access_token = SecureRandom.urlsafe_base64(25).tr('lIO0', 'sxyz')
      self.expired_in = DateTime.now.utc + 7.days
    end while self.class.exists?(access_token: access_token)
  end
end
