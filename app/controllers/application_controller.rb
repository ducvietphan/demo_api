class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  # Devise methods overwrites
  def current_user
    token = Token.find_by_access_token(request.headers['Authorization'])
    @current_user = if token && !token.expired?
                      User.includes(:tokens).where('token.id = ?', token.id).first
                    else
                      nil
                    end
  end

  def authenticate_with_token!
    render json: { errors: "Not authenticated" }, status: :unauthorized unless user_signed_in?
  end

  def user_signed_in?
    current_user.present?
  end
end
