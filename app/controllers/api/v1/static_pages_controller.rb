class Api::V1::StaticPagesController < ApplicationController
  def status
    render json: json_return('OK', '', json_data), status: 200
  end

  private

  def json_data
    {
        StatusCode: 'OK',
        Message: ''
    }
  end

  def json_return(response_code, message, data = '')
    {
        ResponseCode: response_code,
        Message: message,
        Data: data
    }
  end
end
