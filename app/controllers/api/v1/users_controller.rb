class Api::V1::UsersController < ApplicationController
  # before_action :authenticate_with_token!, except: [:create]

  def create
    user = User.new(email: params[:email], password: params[:password])
    if user.save
      render json: { success: 'true' }, status: 200
    else
      render json: { errors: user.errors }, status: 200
    end
  end
end
