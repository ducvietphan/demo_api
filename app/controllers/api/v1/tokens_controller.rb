class Api::V1::TokensController < ApplicationController
  def create
    authorization_code = params[:authorization_code]
    vendor = params[:vendor]

    token = Token.find_by_authorization_code(authorization_code)
    if authorization_code && token
      token.generate_access_token!
      token.update(vendor: vendor, authorization_code: nil)
      token.save
      render json: json_return('OK', '', json_token(token)), status: 200
    else
      render json: json_return('ERROR', 'Invalid authorization code'), status: 200
    end
  end

  def validate
    last_access_token = params[:last_access_token]
    vendor = params[:vendor]

    token = Token.where(access_token: last_access_token, vendor: vendor).first
    if last_access_token && vendor && token
      render json: json_return('OK', '', json_token(token)), status: 200
    else
      render json: json_return('ERROR', 'Invalid access token'), status: 200
    end
  end

  private

  def json_token(token)
    {
      StatusCode: "OK",
      ExprieIn: token.expired_in,
      UserName: "liemvd",
      IsCreator: true,
      IsUnderReview: false,
      Token: token.access_token
    }
  end

  def json_return(response_code, message, data = '')
    {
      ResponseCode: response_code,
      Message: message,
      Data: data
    }
  end
end
