class Api::V1::SessionsController < ApplicationController
  def create
    user_password = params[:password]
    user_email = params[:email]
    user = user_email.present? && User.find_by(email: user_email)

    if user && user.valid_password?(user_password)
      token = user.tokens.create
      render json: json_return('OK', '', token.authorization_code), status: 200
    else
      render json: json_return('ERROR', 'Invalid email or password'), status: 200
    end
  end

  private

  def json_return(response_code, message, data = '')
    {
      ResponseCode: response_code,
      Message: message,
      Data: data
    }
  end
end
