class Api::V1::SystemsController < ApplicationController
  def validate
    render json: json_return('OK', '', json_data), status: 200
  end

  private

  def json_data
    {
        AppVersion: '1.0',
        TermOfService: 'Please note that we will be performing important server maintenance in a few minutes, during which time the server will be unavailable for approximately 5-10 minutes. If you are in the middle of something important, please save your work or hold off on any critical actions until we are finished.'
    }
  end

  def json_return(response_code, message, data = '')
    {
        ResponseCode: response_code,
        Message: message,
        Data: data
    }
  end
end
