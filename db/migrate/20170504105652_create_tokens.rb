class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.references :user, null: false, index: true
      t.string :authorization_code
      t.string :access_token
      t.datetime :expired_in
      t.string :vendor

      t.timestamps null: false
    end
  end
end
